```
jitsi.sf.mpg.de
	version: unknown
	turn_enabled: unknown
	connections tested:
		          proper TLS : working

jitsi.biophys.mpg.de
	version: 4425
	turn_enabled: True
	connections tested:
		          proper TLS : working

meet.jit.si
	version: 4522
	turn_enabled: True
	connections tested:
		          proper TLS : working

jitsi.mpi-bremen.de
	version: 4466
	turn_enabled: True
	connections tested:
		          proper TLS : working

jitsi.mpim-bonn.mpg.de
	version: 3577
	turn_enabled: True
	connections tested:
		          proper TLS : working

jitsi.molgen.mpg.de
	version: 4531
	turn_enabled: True
	connections tested:
		          proper TLS : working

jitsi.khi.fi.it
	version: 4466
	turn_enabled: True
	connections tested:
		          proper TLS : working

jitsi.mpip-mainz.mpg.de
	connections tested:
		          proper TLS : EOF occurred in violation of protocol (_ssl.c:1122)
		      unverified TLS : EOF occurred in violation of protocol (_ssl.c:1122)
		TLS with DH disabled : EOF occurred in violation of protocol (_ssl.c:1122)
		              SSLv23 : EOF occurred in violation of protocol (_ssl.c:1122)

jitsi.pks.mpg.de
	connections tested:
		          proper TLS : EOF occurred in violation of protocol (_ssl.c:1122)
		      unverified TLS : EOF occurred in violation of protocol (_ssl.c:1122)
		TLS with DH disabled : EOF occurred in violation of protocol (_ssl.c:1122)
		              SSLv23 : EOF occurred in violation of protocol (_ssl.c:1122)

jitsi.biochem.mpg.de
	connections tested:
		          proper TLS : [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed: unable to get local issuer certificate (_ssl.c:1122)
		      unverified TLS : Remote end closed connection without response
		TLS with DH disabled : Remote end closed connection without response
		              SSLv23 : Remote end closed connection without response

jitsi.fhi.mpg.de
	version: 3729
	turn_enabled: True
	connections tested:
		          proper TLS : _ssl.c:1105: The handshake operation timed out
		      unverified TLS : [SSL: DH_KEY_TOO_SMALL] dh key too small (_ssl.c:1122)
		TLS with DH disabled : working

jitsi.evolbio.mpg.de
	version: 3729
	turn_enabled: True
	connections tested:
		          proper TLS : [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed: certificate has expired (_ssl.c:1122)
		      unverified TLS : [SSL: DH_KEY_TOO_SMALL] dh key too small (_ssl.c:1122)
		TLS with DH disabled : working

jitsi.mis.mpg.de
	connections tested:
		          proper TLS : timed out
		      unverified TLS : [Errno 110] Connection timed out
		TLS with DH disabled : [Errno 110] Connection timed out
		              SSLv23 : [Errno 110] Connection timed out

jitsi.mpiib-berlin.mpg.de
	connections tested:
		          proper TLS : timed out
		      unverified TLS : [Errno 110] Connection timed out
		TLS with DH disabled : [Errno 110] Connection timed out
		              SSLv23 : [Errno 110] Connection timed out
```
