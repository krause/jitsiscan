import re
import http.client
import ssl
from concurrent.futures import ThreadPoolExecutor, as_completed

DOMAINS = """
meet.jit.si
jitsi.biochem.mpg.de
jitsi.biophys.mpg.de
jitsi.evolbio.mpg.de
jitsi.fhi.mpg.de
jitsi.molgen.mpg.de
jitsi.mpiib-berlin.mpg.de
jitsi.khi.fi.it
jitsi.mpim-bonn.mpg.de
jitsi.mis.mpg.de
jitsi.mpi-bremen.de
jitsi.pks.mpg.de
jitsi.mpip-mainz.mpg.de
jitsi.sf.mpg.de
"""


class JitsiCon():
    def __init__(self, host):
        self.host = host
        self.timeout = 10

    def _connect(self):
        self.conn = http.client.HTTPSConnection(self.host, 443,
                                                timeout=self.timeout)

    def get(self):
        self._connect()
        headers = {
            'Host': self.host,
            'User-Agent': 'curl/7.68.0',
            'Accept': '*/*'
        }
        self.conn.request('GET', '/', headers=headers)
        self.res = self.conn.getresponse()

    def parse(self):
        d = dict(version='unknown', turn_enabled='unknown')
        html = self.res.read().decode()
        ms = re.search(r'libs/lib-jitsi-meet.min.js\?v=(\d+)', html)
        if ms:
            d['version'] = ms.groups()[0]
            ms = re.search('useStunTurn: false', html)  # XXX
            d['turn_enabled'] = not(ms)
        self.conf = d

    def __str__(self):
        return 'proper TLS'


class JitsiUnverifiedCon(JitsiCon):
    def _connect(self):
        ctx = ssl._create_unverified_context()
        self.conn = http.client.HTTPSConnection(self.host, 443,
                                                timeout=None, context=ctx)

    def __str__(self):
        return 'unverified TLS'


class JitsiNoDHCon(JitsiCon):
    def _connect(self):
        ctx = ssl.SSLContext(protocol=ssl.PROTOCOL_TLS)
        ctx.set_ciphers('HIGH MEDIUM !DH')
        self.conn = http.client.HTTPSConnection(self.host, 443, timeout=None,
                                                context=ctx)

    def __str__(self):
        return 'TLS with DH disabled'


class JitsiNoTLS(JitsiCon):
    def _connect(self):
        ctx = ssl.SSLContext(protocol=ssl.PROTOCOL_SSLv23)
        self.conn = http.client.HTTPSConnection(self.host, 443, timeout=None,
                                                context=ctx)

    def __str__(self):
        return 'SSLv23'


def scan(host):
    results = dict(trace=[], host=host)
    for connector in JitsiCon, JitsiUnverifiedCon, JitsiNoDHCon, JitsiNoTLS:
        c = connector(host)
        try:
            c.get()
        except Exception as e:
            results['trace'].append((c, str(e)))
            continue
        c.parse()
        results['conf'] = c.conf
        results['trace'].append((c, 'working'))
        break
    return results


hosts = DOMAINS.strip().split('\n')
with ThreadPoolExecutor(max_workers=len(hosts)) as executor:
    futures = {executor.submit(scan, host): host for host in hosts}
    for future in as_completed(futures):
        data = future.result()
        print(data['host'])
        if 'conf' in data:
            ver = data['conf'].get('version', '')
            turn = data['conf'].get('turn_enabled', '')
            print('\tversion: {}\n\tturn_enabled: {}'.format(ver, turn))
        print('\tconnections tested:')
        for con in data['trace']:
            print('\t\t{:>20} : {}'.format(str(con[0]), con[1]))
        print()
