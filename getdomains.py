import sys
import socket
import re

import http.client
from lxml import html
from concurrent.futures import as_completed, ThreadPoolExecutor

MPG_HOST = 'www.mpg.de'
LIST_PAGE = ('/research_page/11659628/more_items'
             '?category=&letter=&search=&year=&context=institutes&limit=200')
CONCURRENT_REQUESTS = 8  # increase if *very* impatient


def get_website(host, resource):
    """
        find the link to an institute's website from
        their profile page on www.mpg.de
    """
    try:
        conn = http.client.HTTPSConnection(host)
        conn.request('GET', resource)
        res = conn.getresponse()
        page = res.read().decode()
        tree = html.fromstring(page)
        website_node = tree.xpath('//address/div[@class="h3"]/a')[0]
        website = website_node.get('href')
    except Exception as e:
        print(e)
        return 'website not found'
    return website


try:
    conn = http.client.HTTPSConnection(MPG_HOST)
    conn.request('GET', LIST_PAGE)
    res = conn.getresponse()
    page = res.read().decode()
    tree = html.fromstring(page)
except Exception as e:
    print('Error listing MPIs: {}'.format(e))
    sys.exit(1)

with ThreadPoolExecutor(max_workers=CONCURRENT_REQUESTS) as executor:
    futures = []
    for inst in tree.xpath('//div[@class="meta-information"]/h3/a'):
        inst_link = '/' + re.sub('^(https?:)?/?/', '', inst.get('href'))
        futures.append(executor.submit(get_website, MPG_HOST, inst_link))
    for future in as_completed(futures):
        domain = future.result()
        domain = re.sub('^(https?:)?/?/', '', domain)
        domain = re.sub('/$', '', domain)
        domain = 'jitsi.' + domain.replace('www.', '')
        try:
            socket.gethostbyname(domain)
            print(domain)
        except socket.gaierror:
            pass  # no host record found is an expected result
        except Exception as e:
            print('Error for {} : {}'.format(domain, e))
